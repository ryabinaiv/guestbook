﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuestBook.Models;

namespace GuestBook.Repository
{
    public class TestRepository: IRepository
    {
        private List<PostModel> PostsList;
        private int ActualId;
        private const int PostInPageCount = 25;

        public TestRepository()
        {
            ActualId = 0;
            PostsList = new List<PostModel>();
        }

        public int GetPageCount()
        {
            int buf = (int)Math.Truncate((double)ActualId / PostInPageCount);
            return (ActualId % PostInPageCount == 0) ? buf: buf+1;
        }

        public int CreatePostId() => ++ActualId;

        private bool Initialize()
        {
            for (var i = 1; i < 200; i++)
                PostsList.Add(new PostModel(CreatePostId(), $"Name-{i}", $"E-Mail - {i}", null,
                    $"Message - {i}", $"browser - {i}", $"ip-{i}"));
            return true;
        }

        public bool AddPost(PostModel pm)
        {
            PostsList.Add(pm);
            return true;
        }

        private List<PostViewModel> ListToViewModel()
        {
            List<PostViewModel> list = new List<PostViewModel>();
            foreach (var p in PostsList)
                list.Add(new PostViewModel(p.Date, p.UserName, p.EMail, p.MessageText));
            return list;
        }

        public List<PostViewModel> GetPosts(int page, string col, string dir)
        {
            List<PostViewModel> list_for_result = new List<PostViewModel>();
            List<PostViewModel> list_for_ord = ListToViewModel();
            List<PostViewModel> list_ordered = new List<PostViewModel>(); 
            switch (col)
            {
                case "UserName":
                    if(dir == "asc")
                    {
                        list_ordered = (from post in list_for_ord orderby post.UserName select post).ToList();
                        break;
                    }
                    else
                    {
                        list_ordered = (from post in list_for_ord orderby post.UserName descending select post).ToList();
                        break;
                    }      
                case "Date":
                    if (dir == "asc")
                    {
                        list_ordered = (from post in list_for_ord orderby post.Date select post).ToList();
                        break;
                    }
                    else
                    {
                        list_ordered = (from post in list_for_ord orderby post.Date descending select post).ToList();
                        break;
                    }
                case "E-Mail":
                    if (dir == "asc")
                    {
                        list_ordered = (from post in list_for_ord orderby post.EMail select post).ToList();
                        break;
                    }
                    else
                    {
                        list_ordered = (from post in list_for_ord orderby post.EMail descending select post).ToList();
                        break;
                    }
                case "Message":
                    if (dir == "asc")
                    {
                        list_ordered = (from post in list_for_ord orderby post.MessageText select post).ToList();
                        break;
                    }
                    else
                    {
                        list_ordered = (from post in list_for_ord orderby post.MessageText descending select post).ToList();
                        break;
                    }
                default:
                    list_ordered = list_for_ord;
                    break;
            }

            int p = 1;
            int i = 1;
            
            foreach(var item in list_ordered) 
            { 
                if (p == page)
                    list_for_result.Add(item);

                if (i % PostInPageCount == 0)
                    p++;

                i++;
                if (p > page) break;
            }
            return list_for_result;
        }
    }
}

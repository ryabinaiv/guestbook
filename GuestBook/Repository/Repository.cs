﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GuestBook.Models;
using System.Data.SqlClient;

namespace GuestBook.Repository
{
    public interface IRepository
    {
        int CreatePostId();
        bool AddPost(PostModel pm);
        List<PostViewModel> GetPosts(int page, string row, string order);
        int GetPageCount();
    }

    public class SqlRepository : IRepository
    {
        private string ConnectionString;
        private int ActualId;
        private readonly int PostInPageCount;

        public SqlRepository(string connection_string, int posts_count) 
        {
            ConnectionString = connection_string;
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand("SELECT MAX(Id) from Posts", connection);
                try
                {
                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader()) // Call Close when done reading
                    {
                        if (reader.Read())
                            ActualId = (int)reader[0];
                    }
                }
                catch //(Exception e)
                {
                    ActualId = 0;
                }
            }
            PostInPageCount = posts_count;
            Create();
            Initialize(150);
        }

        public int GetPageCount()
        {
            int buf = (int)Math.Truncate((double)ActualId / PostInPageCount);
            return (ActualId % PostInPageCount == 0) ? buf : buf + 1;
        }

        private bool Create()
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand(
                    "CREATE TABLE Posts" +
                    "(" +
                    "Id INT PRIMARY KEY, " +
                    "Date DATE NOT NULL, " +
                    "UserName NCHAR(50) NOT NULL," +
                    "EMail NCHAR (50) NOT NULL, " +
                    "Homepage NCHAR(200) NULL, " +
                    "Message NCHAR(500) NOT NULL, " +
                    "Browser NCHAR(200) NOT NULL, " +
                    "IP NCHAR(50) NOT NULL" +
                    ");", connection);
                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            return true;
        }

        private bool Initialize(int count)
        {
            for (int i = 1; i <= count; ++i)
            {
                if (!AddPost(new PostModel(CreatePostId(), $"Имя{i}", $"E-Mail-{i}", "hp", $"Сообщение({i})", "browser", $"ip-{i}")))
                    return false;
            }
            return true;    
        }

        public int CreatePostId() => ++ActualId;

        public bool AddPost(PostModel pm)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                SqlCommand command = new SqlCommand("INSERT INTO Posts(Id, Date, UserName, EMail, Homepage, Message, Browser, IP)" +
                    " VALUES(@id, @date, @name, @email, @homepage, @message, @browser, @ip)", connection);
                command.Parameters.Add(new SqlParameter("@id", pm.Id));
                command.Parameters.Add(new SqlParameter("@date", pm.Date));
                command.Parameters.Add(new SqlParameter("@name", pm.UserName));
                command.Parameters.Add(new SqlParameter("@email", pm.EMail));
                command.Parameters.Add(new SqlParameter("@homepage", pm.Homepage));
                command.Parameters.Add(new SqlParameter("@message", pm.MessageText));
                command.Parameters.Add(new SqlParameter("@browser", pm.Browser));
                command.Parameters.Add(new SqlParameter("@ip", pm.IP));
                try
                {
                    connection.Open();
                    int result = command.ExecuteNonQuery();
                    if (result == 1) return true;
                    else return false;
                }
                catch //(Exception e)
                {
                    return false;
                }
            }
        }

        public List<PostViewModel> GetPosts(int page, string col, string dir)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                var list = new List<PostViewModel>();
                int start = (page-1) * PostInPageCount + 1;
                int end = start + PostInPageCount - 1;
                string cmd = $"SELECT Date, UserName, EMail, Message, rn FROM ( SELECT row_number() OVER (ORDER BY {col} {dir}) as rn , * FROM Posts ) t WHERE rn BETWEEN {start} AND {end}";
                SqlCommand command = new SqlCommand(cmd, connection);
                try
                {
                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader()) // Call Close when done reading
                    {
                        while (reader.Read())
                        {
                            list.Add(new PostViewModel((DateTime)reader[0], (string)reader[1],
                            (string)reader[2], (string)reader[3]));
                        }
                    }
                    return list;
                }
                catch //(Exception e)
                {
                    return null;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuestBook.Models
{
    public class PostModel
    {
        public int Id { get; }
        public DateTime Date { get; }
        public string UserName { get; }
        public string EMail { get; }
        public string Homepage { get; }
        public string MessageText { get; }
        public string Browser { get; }
        public string IP { get; }

        public PostModel (int id, string user_name, string email, string homepage, 
                            string message_text, string browser, string ip)
        {
            Id = id;
            Date = DateTime.Now;
            UserName = user_name;
            EMail = email;
            Homepage = homepage;
            MessageText = message_text;
            Browser = browser;
            IP = ip;
        }
    }
}

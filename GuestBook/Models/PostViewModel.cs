﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GuestBook.Models
{
    /// <summary>
    /// Для передачи на сайт необходимой информации (без лишнего)
    /// </summary>
    public class PostViewModel
    {
        public DateTime Date;
        public string UserName { get; }
        public string EMail { get; }
        public string MessageText { get; }

        public PostViewModel(DateTime date, string user_name, string email, string message_text)
        {
            Date = date;
            UserName = user_name;
            EMail = email;
            MessageText = message_text;
        }
    }
}

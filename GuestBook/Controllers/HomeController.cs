﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks; 
using Microsoft.AspNetCore.Mvc;
using GuestBook.Models;
using GuestBook.Repository;

namespace GuestBook.Controllers
{
    public class HomeController : Controller
    {                
        [HttpGet]
        public IActionResult Index()
        {
            return RedirectToAction("Index", "Book");
        }
        
        public IActionResult About()
        {
            ViewData["Message"] = "Описание тестового задания";
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

﻿using
    System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using GuestBook.Models;
using GuestBook.Repository;

namespace GuestBook.Controllers
{
    public class BookController: Controller
    {
        private static IRepository repository;
        private static string LastColOrd;
        private static string LastDirOrd;
        private Dictionary<string, string> DicForView = new Dictionary<string, string>
            {
                {"UserName","Имя пользователя" },
                {"Date", "Дата публикации" },
                {"EMail", "E-Mail" },
                {"Message", "Сообщение" },
                {"asc", "Возрастание" },
                {"desc", "Убывание"}
            };

        static BookController()
        {
            repository = new SqlRepository("Data Source=(LocalDb)\\MSSQLLocalDB;Initial Catalog=GuestBookDB;AttachDBFilename=GuestBook.mdf", 25);
            LastColOrd = "Date";
            LastDirOrd = "asc";
        }
        
        [HttpGet]
        public IActionResult Index()
        {
            LastColOrd = "Date";
            LastDirOrd = "asc";
            ViewData["NowPage"] = 1;
            ViewData["PageCount"] = repository.GetPageCount();
            ViewData["Column"] = "Date";
            ViewData["Direction"] = "asc";
            ViewData["ColumnName"] = DicForView[LastColOrd];
            ViewData["DirectionName"] = DicForView[LastDirOrd];
            return View("Page", repository.GetPosts(1, "Date", "asc")??new List<PostViewModel>());
        }

        [HttpGet]
        public IActionResult Page(int page, string column, string direction)
        {
            if (page > repository.GetPageCount())
                page = 1;
            if (!DicForView.Keys.Contains(column??""))
                column = "Date";
            if (!DicForView.Keys.Contains(direction??""))
                column = "asc";
            ViewData["ColumnName"] = DicForView[LastColOrd];
            ViewData["DirectionName"] = DicForView[LastDirOrd];
            ViewData["NowPage"] = page;
            ViewData["PageCount"] = repository.GetPageCount();
            ViewData["Column"] = column;
            ViewData["Direction"] = direction;
            return View(repository.GetPosts(page, column, direction) ?? new List<PostViewModel>());
        }

        [HttpPost]
        public IActionResult Page(string col, string dir)
        {
            if (col == "last")
                col = LastColOrd;
            if (dir == "last")
                dir = LastDirOrd;
            LastColOrd = col;
            LastDirOrd = dir;
            ViewData["Column"] = DicForView[LastColOrd];
            ViewData["Direction"] = DicForView[LastDirOrd];
            return RedirectToAction("Page", new { page = 1, column = col, direction = dir });
        }

        [HttpPost]
        public IActionResult NewPost(string UserName, string EMail, string Homepage, string Message)
        {
            if (Homepage == null)
                Homepage = "";
            string ip = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            string browser = Request.Headers["User-Agent"];
            if (repository.AddPost(new PostModel(repository.CreatePostId(), UserName, EMail, Homepage, Message, browser, ip)))
                return RedirectToAction("Page", new { page = repository.GetPageCount(), column = "Date", direction = "asc"});
            else
                return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult NewPost()
        {
            return View();
        }
    }
}
